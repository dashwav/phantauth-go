# PhantAuth Go API Wrapper

PhantAuth-Go is an API wrapper for the random user generator provided from https://www.phantauth.net/doc/generator.

This allows for deterministic random user generation that can assist with testing oauth/user integrations.

Currently PhantAuth supports both Random and Customized User generation, as well as Random and Customized Team generation.

### Example Usage:
```
httpClient := &http.Client{
    Transport:     nil,
    CheckRedirect: nil,
    Jar:           nil,
    Timeout:       60 * time.Second,
}
client, err := NewClient(httpClient)
if err != nil {
    t.Error(err)
    return
}
user, err := client.NewUser(&UserOpts{
    FirstName:  "John",
    MiddleName: "J",
    LastName:   "Smith",
    Gender:     "male",
    Avatar:     "sketch",
})
if err != nil {
    t.Error(err)
    return
}
```

Which would generate a user like this:
```json
{
  "locale": "en_GB",
  "profile": "https://phantauth.net/user/john.j.smith;male;sketchjohn.j.smith%3Bsketch%3Bmale/profile",
  "sub": "john.j.smith;sketch;male",
  "nickname": "John",
  "preferred_username": "jsmith",
  "picture": "https://www.gravatar.com/avatar/55ee9c3d94698b52e8b0083a79fa402d?s=256&d=https://avatars.phantauth.net/sketch/male/GRb4kgaB.jpg",
  "website": "https://phantauth.net",
  "family_name": "Smith",
  "given_name": "John",
  "middle_name": "J",
  "email": "john.j.smith.X5KX3UA@mailinator.com",
  "email_verified": true,
  "gender": "male",
  "birthdate": "1987-10-31",
  "zoneinfo": "Europe/London",
  "phone_number": "555-236-3300",
  "phone_number_verified": true,
  "updated_at": 1572480000,
  "me": "https://phantauth.net/~john.j.smith%3Bsketch%3Bmale",
  "password": "dQfn133o",
  "webmail": "https://www.mailinator.com/v3/?zone=public&query=john.j.smith.X5KX3UA",
  "uid": "Ax60k1/e78o",
  "address": {
    "formatted": "56 Cove Lane APT 183\nSan Francisco 40309",
    "street_address": "56 Cove Lane APT 183",
    "locality": "San Francisco",
    "postal_code": "40309",
    "country": "UnitedKingdom"
  },
  "name": "John Smith",
  "@id": "https://phantauth.net/user/john.j.smith%3Bsketch%3Bmale"
}
```