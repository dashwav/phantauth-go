package phantauth

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

// Team is a group of users under a given name. For the purposes of identification and visualisation, the team object
// has its own properties (e.g. logo), the most important of which is the members, which contains the user objects of the team.
type Team struct {
	// The name or email address of a given team.
	// The team properties and team members are generated from this name.
	Sub string
	// The displayed team name.
	Name string
	// The URL of the team logo, which can be customized by the gravatar associated with the email address
	// in the logo_email property.
	Logo string
	// The email address of the team, either generated or provided in the sub property.
	// The team logo can be customized by the use of the gravatar associated with this email address.
	LogoEmail string `json:"logo_email"`
	// URL of the Teams's JSON representation.
	AtID string `json:"@id"`
	// The URL of the Team profile.
	Profile string
	//  The user objects that generate a team member.
	Members []*User
}

// NewTeamRandom generates a team using the default random variables
func (c *Client) NewTeamRandom() (*Team, error) {
	url := fmt.Sprintf("%s/%s/", c.baseURL, "team")
	resp, err := c.httpClient.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New(fmt.Sprintf("API returned a non-200 status code: %d", resp.StatusCode))
	}

	response := &Team{}
	err = json.NewDecoder(resp.Body).Decode(response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

// TeamOpts contains options for customizing a generated team.
type TeamOpts struct {
	// Name of the team
	Name string
	// Email address of the team
	Email string
	// Logo customizations. Possible values: "icon" "fractal"
	Logo string
	// Team Size options.
	// "tiny" 5 members
	// "small" 10 members
	// "medium" 25 members
	// "large" 50 members
	// "huge" 100 members
	Size string
}

// NewTeam generates a team with the given options passed in.
// If nil passed in instead of options, NewTeam behaves like NewTeamRandom
func (c *Client) NewTeam(opts *TeamOpts) (*Team, error) {
	if opts == nil {
		return c.NewTeamRandom()
	}
	teamName, err := parseTeamOpts(opts)
	if err != nil {
		return nil, err
	}
	url := fmt.Sprintf("%s/%s/%s", c.baseURL, "team", teamName)
	resp, err := c.httpClient.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New(fmt.Sprintf("API returned a non-200 status code: %d", resp.StatusCode))
	}

	response := &Team{}
	err = json.NewDecoder(resp.Body).Decode(response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func parseTeamOpts(opts *TeamOpts) (string, error) {
	retString := opts.Name
	if len(retString) == 0 && len(opts.Email) > 0 {
		retString = opts.Email
	}
	if len(retString) == 0 {
		return retString, errors.New("no name or email provided")
	}
	if len(opts.Logo) > 0 {
		retString += fmt.Sprintf(";%s", opts.Logo)
	}
	if len(opts.Size) > 0 {
		retString += fmt.Sprintf(";%s", opts.Size)
	}
	return retString, nil
}
