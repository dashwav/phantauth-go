package phantauth

import "testing"

func TestGetRandomUser(t *testing.T) {
	client, err := NewClient(httpClient)
	if err != nil {
		t.Error(err)
		return
	}
	_, err = client.NewUserRandom()
	if err != nil {
		t.Error(err)
		return
	}
}

func TestName(t *testing.T) {
	client, err := NewClient(httpClient)
	if err != nil {
		t.Error(err)
		return
	}
	user, err := client.NewUser(&UserOpts{
		FirstName:  "John",
		MiddleName: "J",
		LastName:   "Smith",
		Gender:     "male",
		Avatar:     "sketch",
	})
	if err != nil {
		t.Error(err)
		return
	}
	if g := user.Gender; g != "male" {
		t.Errorf("Male flag was passed in, got %s instead", user.Gender)
	}
	if f := user.GivenName; f != "John" {
		t.Errorf("Given name was passed in as John, got %s instead", user.GivenName)
	}
	if m := user.MiddleName; m != "J" {
		t.Errorf("Middle name was passed in as J, got %s instead", user.MiddleName)
	}
	if l := user.FamilyName; l != "Smith" {
		t.Errorf("Last name was passed in as Smith, got %s instead", user.FamilyName)
	}
}

func TestEmail(t *testing.T) {
	client, err := NewClient(httpClient)
	if err != nil {
		t.Error(err)
		return
	}
	user, err := client.NewUser(&UserOpts{
		Email: "test.user@testemail.com",
	})
	if err != nil {
		t.Error(err)
		return
	}
	if f := user.GivenName; f != "Test" {
		t.Errorf("Given name was passed in as Test, got %s instead", user.GivenName)
	}
	if l := user.FamilyName; l != "User" {
		t.Errorf("Last name was passed in as User, got %s instead", user.FamilyName)
	}
	if e := user.Email; e != "test.user@testemail.com" {
		t.Errorf("Email was passed in as test.user@testemail.com, got %s instead", user.Email)
	}
}

func TestNameEmailPrecedence(t *testing.T) {
	client, err := NewClient(httpClient)
	if err != nil {
		t.Error(err)
		return
	}
	user, err := client.NewUser(&UserOpts{
		FirstName:  "John",
		MiddleName: "J",
		LastName:   "Smith",
		Email:      "test.user@testemail.com",
	})
	if err != nil {
		t.Error(err)
		return
	}
	if f := user.GivenName; f != "John" {
		t.Errorf("Given name was passed in as John, got %s instead", user.GivenName)
	}
	if m := user.MiddleName; m != "J" {
		t.Errorf("Middle name was passed in as J, got %s instead", user.MiddleName)
	}
	if l := user.FamilyName; l != "Smith" {
		t.Errorf("Last name was passed in as Smith, got %s instead", user.FamilyName)
	}
	if e := user.Email; e == "test.user@testemail.com" {
		t.Errorf("Email was was not overriden, got %s", user.Email)
	}
}

func TestNoNameOpts(t *testing.T) {
	client, err := NewClient(httpClient)
	if err != nil {
		t.Error(err)
		return
	}
	_, err = client.NewUser(&UserOpts{
		Gender: "male",
	})
	if err == nil {
		t.Errorf("Passed in incorrect format, expected error")
	}
}
