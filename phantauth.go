// Package phantauth wraps the Phantauth random user generator
package phantauth

import (
	"net/http"
)

// Base Address is the root of the PhantAuth API
const baseAddress = "https://phantauth.net"

// Client is the client for interacting with the api. It is created using `NewClient`
type Client struct {
	httpClient *http.Client
	baseURL    string
}

// NewClient returns a client with the given `http.Client`
func NewClient(client *http.Client) (*Client, error) {
	return &Client{
		httpClient: client,
		baseURL:    baseAddress,
	}, nil
}
