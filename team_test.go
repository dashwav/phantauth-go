package phantauth

import "testing"

func TestGetRandomTeam(t *testing.T) {
	client, err := NewClient(httpClient)
	if err != nil {
		t.Error(err)
		return
	}
	_, err = client.NewTeamRandom()
	if err != nil {
		t.Error(err)
		return
	}
}

func TestTeamName(t *testing.T) {
	client, err := NewClient(httpClient)
	if err != nil {
		t.Error(err)
		return
	}
	team, err := client.NewTeam(&TeamOpts{
		Name: "test.team",
	})
	if err != nil {
		t.Error(err)
		return
	}
	if l := team.Name; l != "Test Team" {
		t.Errorf("Last name was passed in as Test Team, got %s instead", team.Name)
	}
}

func TestTeamEmail(t *testing.T) {
	client, err := NewClient(httpClient)
	if err != nil {
		t.Error(err)
		return
	}
	team, err := client.NewTeam(&TeamOpts{
		Email: "testteam@email.com",
	})
	if err != nil {
		t.Error(err)
		return
	}
	if l := team.Name; l != "Testteam" {
		t.Errorf("Last name was passed in as Testteam, got %s instead", team.Name)
	}
	if l := team.LogoEmail; l != "testteam@email.com" {
		t.Errorf("Last name was passed in as testteam@email.com, got %s instead", team.LogoEmail)
	}
}

func TestTeamSize(t *testing.T) {
	client, err := NewClient(httpClient)
	if err != nil {
		t.Error(err)
		return
	}
	team, err := client.NewTeam(&TeamOpts{
		Email: "testteam@email.com",
		Size:  "medium",
	})
	if err != nil {
		t.Error(err)
		return
	}
	if n := team.Name; n != "Testteam" {
		t.Errorf("Last name was passed in as Testteam, got %s instead", team.Name)
	}
	if l := team.LogoEmail; l != "testteam@email.com" {
		t.Errorf("Last name was passed in as testteam@email.com, got %s instead", team.LogoEmail)
	}
	if mem := team.Members; len(mem) != 25 {
		t.Errorf("Medum team (25 members) was requested, got team of size %d instead", len(team.Members))
	}
}

func TestTeamNameEmailPrecedence(t *testing.T) {
	client, err := NewClient(httpClient)
	if err != nil {
		t.Error(err)
		return
	}
	team, err := client.NewTeam(&TeamOpts{
		Name:  "test.xyz",
		Email: "testteam@email.com",
	})
	if err != nil {
		t.Error(err)
		return
	}
	if l := team.Name; l != "Test Xyz" {
		t.Errorf("Last name was passed in as Test Xyz, got %s instead", team.Name)
	}
	if l := team.Name; l == "Testteam" {
		t.Errorf("Last name was passed in as Testteam, got %s instead", team.Name)
	}
	if l := team.LogoEmail; l == "testteam@email.com" {

		t.Errorf("Email was was not overriden, got %s", team.LogoEmail)
	}
}

func TestTeamNoNameOpts(t *testing.T) {
	client, err := NewClient(httpClient)
	if err != nil {
		t.Error(err)
		return
	}
	_, err = client.NewTeam(&TeamOpts{
		Name:  "",
		Email: "",
	})
	if err == nil {
		t.Errorf("Passed in incorrect format, expected error")
	}
}
