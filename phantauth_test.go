package phantauth

import (
	"net/http"
	"testing"
	"time"
)

var httpClient = &http.Client{
	Transport:     nil,
	CheckRedirect: nil,
	Jar:           nil,
	Timeout:       60 * time.Second,
}

func TestNewClient(t *testing.T) {
	_, err := NewClient(httpClient)
	if err != nil {
		t.Error(err)
	}
}