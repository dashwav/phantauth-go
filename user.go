package phantauth

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

// User is the base object of the PhantAuth Generator
type User struct {
	// User identifier at the issuer.
	Sub string
	// The user's full name in displayable form, including all name parts, possibly including titles and suffixes,
	// ordered according to the enduser's locale and preferences.
	Name string
	//  The user's preferred postal address.
	Address Address
	// The user's given name(s) or first name(s).
	GivenName string `json:"given_name"`
	// The user's surname(s) or last name(s).
	FamilyName string `json:"family_name"`
	// The user's middle name(s).
	MiddleName string `json:"middle_name"`
	//  A casual name of the User that may or may not be the same as the given_name.
	Nickname string
	// A shorthand name by which the user wishes to be referred to at the Relying Party.
	PreferredUsername string `json:"preferred_username"`
	// The URL of the user's profile page.
	Profile string
	// The URL of the user's profile picture.
	Picture string
	// The URL of the user's webpage or blog
	Website string
	// The user's preferred email address.
	Email string
	// True if the user's e-mail address has been verified; otherwise false
	EmailVerified bool `json:"email_verified"`
	// The enduser's gender. Possible values are: female, male, and unknown.
	Gender string
	// The user's birthday, represented as an ISO 8601:2004 [ISO8601‑2004] YYYY-MM-DD format.
	Birthdate string
	// A string from the zoneinfo time zone database representing the user's time zone.
	// For example, Europe/Paris or America/Los_Angeles.
	ZoneInfo string
	// The user's locale, represented as a BCP47 [RFC5646] language tag.
	// It is an ISO 639-1 Alpha-2 language code in lowercase and an
	// ISO 3166-1 Alpha-2 country code in uppercase letters,separated by a dash.
	Locale string
	// The user's preferred telephone number
	PhoneNumber string `json:"phone_number"`
	// True if the enduser's phone number has been verified; otherwise false.
	PhoneNumberVerified bool `json:"phone_number_verified"`
	// The time when the User's information was last updated.
	// Its value is a JSON number representing the number of seconds from 1970-01-01T0:0:0Z
	// as measured in UTC until the date/time.
	UpdatedAt int64 `json:"updated_at"`
	// The simplified URL of the user's profile page.
	Me string
	// The user's generated password.
	Password string
	// The user's simplified, shortened identifier at the Issuer.
	Uid string
	//  The URL of user's mailbox in a webmail application.
	Webmail string
	// The URL of the user's JSON representation.
	AtID string `json:"@id"`
}

// Address is a representation of a generated users address
type Address struct {
	// Full mailing address, formatted for display or use on a mailing label.
	// This field MAY contain multiple lines, separated by newlines.
	// Newlines can be represented either as a carriage return/line feed pair or as a single line feed character.
	Formatted string
	// Full street address component, which MAY include house number, street name, post office box,
	// and multi-line extended street address information. This field MAY contain multiple lines, separated by newlines.
	// Newlines can be represented either as a carriage return/line feed pair or as a single line feed character.
	StreetAddress string `json:"street_address"`
	// City or locality component.
	Locality string
	// State, province, prefecture, or region component.
	Region string
	// Zip code or postal code component.
	PostalCode string `json:"postal_code"`
	// Country name component.
	Country string
}

// Generates a new User object from API using the default random values
func (c *Client) NewUserRandom() (*User, error) {
	url := fmt.Sprintf("%s/%s/", c.baseURL, "user")
	resp, err := c.httpClient.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New(fmt.Sprintf("API returned a non-200 status code: %d", resp.StatusCode))
	}

	response := &User{}
	err = json.NewDecoder(resp.Body).Decode(response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

// UserOpts contains optional parmaeters that can be used when creating a new user.
// Either Firstname or Email are required, all other fields are optional
type UserOpts struct {
	// Required: User's first name
	FirstName string
	// User's middle name
	MiddleName string
	// User's Last Name
	LastName string
	// User's Gender. Possible values are: "female", "male", and "unknown".
	Gender string
	// Type of Avatar. Possible values can be found here: https://www.phantauth.net/doc/generator#user-avatar-flags
	Avatar string
	// Required: User's email. First/middle/last will be infered from email with `.` seperators
	// e.g. first.middle.last@email.com will generate a user with name "First Middle Last"
	Email string
}

// Generates a user with the given options passed in.
// If nil passed in instead of options, NewUser behaves like NewUserRandom
func (c *Client) NewUser(opts *UserOpts) (*User, error) {
	if opts == nil {
		return c.NewUserRandom()
	}
	username, err := parseUserOpts(opts)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("%s/%s/%s", c.baseURL, "user", username)
	resp, err := c.httpClient.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New(fmt.Sprintf("API returned a non-200 status code: %d", resp.StatusCode))
	}

	response := &User{}
	err = json.NewDecoder(resp.Body).Decode(response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func parseUserOpts(opts *UserOpts) (string, error) {

	retString := buildName(opts.FirstName, opts.MiddleName, opts.LastName)
	if len(retString) == 0 && len(opts.Email) > 0 {
		retString = opts.Email
	}
	if len(retString) == 0 {
		return retString, errors.New("no name or email provided")
	}
	if len(opts.Gender) > 0 {
		retString += fmt.Sprintf(";%s", opts.Gender)
	}
	if len(opts.Avatar) > 0 {
		retString += fmt.Sprintf(";%s", opts.Avatar)
	}
	return retString, nil
}

func buildName(first string, middle string, last string) string {
	retString := ""
	if len(first) > 0 && len(middle) > 0 && len(last) > 0 {
		retString = fmt.Sprintf("%s.%s.%s", first, middle, last)
	} else if len(first) > 0 && len(last) > 0 {
		retString = fmt.Sprintf("%s.%s", first, last)
	} else if len(first) > 0 {
		retString = fmt.Sprintf("%s", first)
	}
	return retString
}
